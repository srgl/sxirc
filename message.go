package main

import (
	"errors"
	"strings"
)

type (
	Message struct {
		Tags       map[string]string
		Source     string
		Command    string
		Parameters []string
	}
	Nickmask struct {
		Nick     string
		User     string
		Hostname string
	}
)

func ParseMessage(input string) (msg *Message, err error) {
	lx := NewLexer(input)
	msg = &Message{}
	msg.Tags = make(map[string]string)

	for i := lx.NextItem(); i.kind != ItemEOL; i = lx.NextItem() {
		switch i.kind {
		case ItemTag:
			if idx := strings.IndexByte(lx.input[i.start:i.end], '='); idx >= 0 {
				msg.Tags[lx.input[i.start:idx+1]] = lx.input[idx+2 : i.end]
			} else {
				msg.Tags[lx.input[i.start:i.end]] = ""
			}
		case ItemSource:
			msg.Source = lx.input[i.start:i.end]
		case ItemCommand:
			msg.Command = lx.input[i.start:i.end]
		case ItemParameter:
			msg.Parameters = append(msg.Parameters, lx.input[i.start:i.end])
		case ItemError:
			err = errors.New(i.err)
			return msg, err
		}
	}

	return msg, nil
}

func (m Message) String() string {
	var str strings.Builder
	if m.Tags != nil {
		i := 0
		str.WriteRune('@')
		for k, v := range m.Tags {
			if i != 0 {
				str.WriteRune(';')
			}
			str.WriteString(k)
			if v != "" {
				str.WriteRune('=')
				str.WriteString(v)
			}
			i++
		}
	}
	if m.Source != "" {
		str.WriteString(" :")
		str.WriteString(m.Source)
	}
	str.WriteRune(' ')
	str.WriteString(m.Command)

	if len(m.Parameters) > 0 {
		str.WriteRune(' ')
		for i, v := range m.Parameters {
			if i != 0 {
				str.WriteRune(' ')
			}
			if (i == len(m.Parameters)-1) && (strings.IndexRune(v, ' ') >= 0) {
				str.WriteRune(':')
			}
			str.WriteString(v)
		}
	}

	return str.String()
}

func ParseNickmask(input string) string {

	return ""
}
