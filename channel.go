package main

import (
	"fmt"
	"strings"
)

type (
	Channel struct {
		Name  string
		Topic string
		Users []string

		parent *Server
	}
)

func (srv *Server) JoinChannel(cn string) error {
	if (strings.IndexByte(cn, '#') == -1) && (strings.IndexByte(cn, '&') == -1) {
		return ServerError(srv, "Invalid channel name \"%s\"", cn)
	}

	ch := &Channel{}
	ch.Name = cn
	ch.parent = srv

	err := srv.Send(fmt.Sprintf("JOIN %s", cn))
	if err != nil {
		return err
	}

	_ = srv.Wait(func(msg Message) (bool, error) {
		switch msg.Command {
		case "332":
			// topic
			ch.Topic = msg.Parameters[3]
			return false, nil
		case "353":
			ch.Users = append(ch.Users, strings.Split(msg.Parameters[3], " ")...)
		default:
			if ch.Users == nil {
				return false, nil
			} else {
				return true, nil
			}
		}
		return false, nil
	})

	srv.Channels[cn] = ch

	return nil
}
