package main

import (
	"encoding/base64"
	"fmt"
	"io"
	"strings"
)

type SASLType int

const (
	SASLNone SASLType = iota
	SASLPlain
)

type SASLMethod interface {
	Authenticate(srv *Server) (bool, error)
}

type AuthPlain struct {
	AuthID, Pass string
}

func (a AuthPlain) Authenticate(srv *Server) (res bool, err error) {
	var str strings.Builder
	var bstr strings.Builder

	str.WriteString(a.AuthID)
	str.WriteByte(0)
	str.WriteString(a.AuthID)
	str.WriteByte(0)
	str.WriteString(a.Pass)

	b64 := base64.NewEncoder(base64.StdEncoding, &bstr)
	defer func(b64 io.WriteCloser) {
		err := b64.Close()
		if err != nil {
			panic(err)
		}
	}(b64)
	_, err = b64.Write([]byte(str.String()))
	if err != nil {
		return false, err
	}

	err = srv.Send("AUTHENTICATE PLAIN")
	if err != nil {
		return false, err
	}

	_ = srv.Wait(func(m Message) (bool, error) {
		if m.Command == "AUTHENTICATE" && m.Parameters[0] == "+" {
			return true, nil
		}
		return false, nil
	})

	err = srv.Send(fmt.Sprintf("AUTHENTICATE %s", bstr.String()))
	if err != nil {
		return false, err
	}

	err = srv.Wait(func(m Message) (bool, error) {
		switch m.Command {
		case "903":
			return true, nil
		case "902", "904", "905", "906", "907":
			return true, ServerError(srv, "%s", m.Parameters[1])
		default:
			return false, nil
		}
	})
	if err != nil {
		return false, err
	}

	return
}
