package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

type (
	ItemType int8
	Item     struct {
		kind  ItemType
		start int
		end   int
		err   string
	}
	Lexer struct {
		input    string
		start    int
		position int
		width    int
		items    chan Item
		state    StateFn
	}
	StateFn func(*Lexer) StateFn
)

const EOF = rune(0)

const (
	ItemError ItemType = iota - 1
	ItemEOL
	ItemTag
	ItemSource
	ItemCommand
	ItemParameter
)

func (it ItemType) String() string {
	return []string{"Error", "EOL", "Tag", "Source", "Command", "Parameter"}[it+1]
}
func (i Item) String() string {
	if i.err != "" {
		return fmt.Sprintf("ERROR: %s", i.err)
	} else {
		return fmt.Sprintf("%s [%d:%d]", i.kind, i.start, i.end)
	}
}

func NewLexer(input string) *Lexer {
	lx := &Lexer{
		input: input,
		items: make(chan Item, 2),
		state: lexState_start,
	}
	return lx
}
func (l *Lexer) Error(format string, args ...interface{}) Item {
	return Item{
		kind: ItemError,
		err:  fmt.Sprintf(format, args...),
	}
}
func (l *Lexer) Emit(it ItemType) {
	i := Item{kind: it, start: l.start, end: l.position}
	l.items <- i
	l.start = l.position
}

func (l *Lexer) NextItem() Item {
	for {
		select {
		case i := <-l.items:
			return i
		default:
			if l.state == nil {
				l.Emit(ItemEOL)
			} else {
				l.state = l.state(l)
			}
		}
	}
}

/* utility functions for the lexer */

func (l *Lexer) EOF() bool {
	return l.position >= len(l.input)
}
func (l *Lexer) next() (r rune) {
	if l.position >= len(l.input) {
		l.width = 0
		return EOF
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.position:])
	l.position += l.width
	return r
}
func (l *Lexer) rewind() {
	l.position -= l.width
}
func (l *Lexer) peek() (r rune) {
	r = l.next()
	l.rewind()
	return r
}
func (l *Lexer) acceptUntil(chars string) {
	for strings.IndexRune(chars, l.next()) == -1 {
		if l.EOF() {
			l.Error("expected any of '%s', got EOF", string(chars))
			return
		}
	}
	l.rewind()
}
func (l *Lexer) acceptUntilEOF() {
	for !l.EOF() {
		l.next()
	}
}
func (l *Lexer) skip() {
	l.start++
	l.position++
}
func (l *Lexer) eatWhitespace() {
	for l.peek() == ' ' {
		l.skip()
	}
}

func lexState_start(l *Lexer) StateFn {
	l.eatWhitespace()

	switch l.peek() {
	case '@':
		return lexState_tag
	case ':':
		return lexState_source
	default:
		return lexState_command
	}
}
func lexState_eol(l *Lexer) StateFn {
	l.Emit(ItemEOL)

	return nil
}
func lexState_tag(l *Lexer) StateFn {
	l.skip()
	if strings.Index(l.input[l.start:], ";") >= 0 {
		l.acceptUntil(";")
		l.Emit(ItemTag)
		return lexState_tag
	} else {
		l.acceptUntil(" ")
		l.Emit(ItemTag)
		return lexState_start
	}
}
func lexState_source(l *Lexer) StateFn {
	l.skip() // Skip start character
	l.acceptUntil(" ")
	l.Emit(ItemSource)
	return lexState_start
}
func lexState_command(l *Lexer) StateFn {
	if l.EOF() {
		l.Error("expected command, got EOF")
	}

	l.acceptUntil(" ")
	l.Emit(ItemCommand)
	if l.EOF() { return lexState_eol }
	return lexState_parameter
}
func lexState_parameter(l *Lexer) StateFn {
	l.eatWhitespace()
	if l.peek() == ':' {
		return lexState_trailingParameter
	} else {
		l.acceptUntil(" ")
		l.Emit(ItemParameter)
		if !l.EOF() {
			return lexState_parameter
		}
	}

	return lexState_eol
}
func lexState_trailingParameter(l *Lexer) StateFn {
	l.skip() // Skip start character
	l.acceptUntilEOF()
	l.Emit(ItemParameter)
	return lexState_eol
}
