package main

import (
	"fmt"
	"log"
)

type IRCError struct {
	context struct {
		server  *Server
		channel *Channel
		message *Message
	}
	what string
}

func (err IRCError) Error() string {
	if err.context.channel != nil {
		return fmt.Sprintf("server %s, channel %s: %s", err.context.server.Network, err.context.channel.Name, err.what)
	}
	return fmt.Sprintf("server %s: %s", err.context.server.Network, err.what)
}

func (err IRCError) Log() {
	if err.context.server.debug {
		log.Println(err.Error())
	}

	/* TODO: log to whatever */
}

func ServerError(srv *Server, format string, args ...interface{}) IRCError {
	return IRCError{
		context: struct {
			server  *Server
			channel *Channel
			message *Message
		}{
			server: srv,
		},
		what: fmt.Sprintf(format, args...),
	}
}
