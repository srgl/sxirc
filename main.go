/* sxirc: sergal experimental IRC client */

package main

import (
	"fmt"
	"os"
)

func main() {
	srv := NewServer("localhost", 6697)
	defer srv.Close()

	srv.Option(
		EnableTLS,
		EnableSASL(SASLPlain, AuthPlain{"test", "testpass"}),
		Debug,
	)

	err := srv.Connect()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	err = srv.JoinChannel("#meta")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	fmt.Println(srv.Channels["#meta"].Users)
}
