package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

type (
	Identity struct {
		Username, Nickname, Realname string
	}
	Server struct {
		Network      string
		Port         int
		TLS          bool
		Identity     Identity
		Capabilities []string

		Password   string
		DoSASL     SASLType
		AuthMethod SASLMethod

		Channels map[string]*Channel

		Writer  io.Writer
		Scanner *bufio.Scanner

		Messages      chan Message
		DiscardedMsgs chan Message
		Errors        chan error

		mutex sync.RWMutex

		socket    net.Conn
		socketTLS *tls.Conn
		debug     bool
	}
	ServerOption func(*Server)
)

func NewServer(network string, port int, options ...ServerOption) (srv *Server) {
	srv = &Server{}
	srv.Network = network
	srv.Port = port
	srv.Channels = make(map[string]*Channel)
	srv.Messages = make(chan Message)
	srv.Errors = make(chan error)
	srv.Option(DefaultIdentity)
	srv.Option(options...)

	return
}

func (srv *Server) Connect() error {
	var err error

	if srv.TLS {
		srv.socketTLS, err = tls.Dial("tcp", fmt.Sprintf("%s:%d", srv.Network, srv.Port), &tls.Config{InsecureSkipVerify: srv.debug})
		if err != nil {
			return err
		}
		srv.Scanner = bufio.NewScanner(srv.socketTLS)
		srv.Writer = srv.socketTLS
	} else {
		srv.socket, err = net.Dial("tcp", fmt.Sprintf("%s:%d", srv.Network, srv.Port))
		if err != nil {
			return err
		}
		srv.Scanner = bufio.NewScanner(srv.socket)
		srv.Writer = srv.socket
	}
	srv.Scanner.Split(scanCRLF)

	go func(s *bufio.Scanner) {
		for s.Scan() {
			txt := s.Text()
			if srv.debug {
				log.Println("<-", txt)
			}
			m, err := ParseMessage(txt)
			if err != nil {
				srv.Errors <- err
				return
			}
			srv.Messages <- *m
		}
		if err := s.Err(); err != nil {
			srv.Errors <- err
			return
		}
	}(srv.Scanner)

	err = srv.Handshake()
	if err != nil {
		return err
	}

	return nil
}
func (srv *Server) Option(opt ...ServerOption) {
	for _, o := range opt {
		o(srv)
	}
}

func (srv *Server) Handshake() error {
	err := srv.Send("CAP LS 302")
	if err != nil {
		return err
	}

capLoop:
	for {
		select {
		case msg := <-srv.Messages:
			if (msg.Command == "421") || (msg.Command != "CAP" && srv.Capabilities != nil) {
				break capLoop
			} else if msg.Command == "CAP" {
				srv.Capabilities = append(srv.Capabilities, strings.Split(msg.Parameters[len(msg.Parameters)-1], " ")...)
			}
		}
	}

	if srv.Password != "" {
		err = srv.Send(fmt.Sprintf("PASS %s", srv.Password))
		if err != nil {
			return err
		}
	}

	err = srv.Send(fmt.Sprintf("NICK %s", srv.Identity.Nickname))
	if err != nil {
		return err
	}

	err = srv.Send(fmt.Sprintf("USER %s 0 * :%s", srv.Identity.Username, srv.Identity.Realname))
	if err != nil {
		return err
	}

	if srv.Capabilities != nil {
		sasl, _, err := srv.requestCap("sasl")
		if err != nil {
			return err
		}
		if sasl {
			_, err := srv.AuthMethod.Authenticate(srv)
			if err != nil {
				switch err.(type) {
				case IRCError:
					err.(IRCError).Log()
					goto saslEnd
				default:
					return err
				}
			}
		} else {
			log.Println("WARN: server doesn't support SASL authentication")
		}
	saslEnd:
		err = srv.Send("CAP END")
		if err != nil {
			return err
		}
	}

	/* TODO: handle MOTD and welcome */

	return nil
}
func (srv *Server) Send(msg string) error {
	srv.mutex.Lock()
	defer srv.mutex.Unlock()

	_, err := ParseMessage(msg)
	if err != nil {
		return err
	}
	if srv.debug {
		log.Println("->", msg)
	}
	_, err = srv.Writer.Write([]byte(msg + "\r\n"))
	if err != nil {
		return err
	}
	return nil
}
func (srv *Server) Wait(fn func(msg Message) (bool, error)) error {
	srv.mutex.RLock()
	defer srv.mutex.RUnlock()

	for {
		select {
		case msg := <-srv.Messages:
			fmt.Println(msg)

			res, err := fn(msg)
			if err != nil {
				return err
			} else if res {
				return nil
			} else {
				srv.DiscardedMsgs <- msg
				continue
			}
		case msg := <-srv.DiscardedMsgs:
			fmt.Println("!! discarded:", msg)

			res, err := fn(msg)
			if err != nil {
				return err
			} else if res {
				return nil
			}
		}
	}
}
func (srv *Server) Close() {
	var err error
	if srv.TLS {
		if srv.socketTLS != nil {
			err = srv.socketTLS.Close()
		}
	} else {
		err = srv.socket.Close()
	}
	if err != nil {
		panic(err)
	}
}

func (srv *Server) requestCap(cap string) (ret bool, capr string, err error) {
	err = srv.Send(fmt.Sprintf("CAP REQ %s", cap))
	if err != nil {
		return false, "", err
	}

	for {
		select {
		case msg := <-srv.Messages:
			if msg.Command == "CAP" {
				if msg.Parameters[1] == "ACK" {
					return true, msg.Parameters[2], nil
				} else if msg.Parameters[1] == "NAK" {
					return false, "", nil
				}
			}
		}
	}
}

func scanCRLF(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.Index(data, []byte{'\r', '\n'}); i >= 0 {
		if data[0] == '\n' {
			return i + 1, data[1:i], nil
		} else {
			return i + 1, data[0:i], nil
		}
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func Debug(s *Server) {
	s.debug = true
}
func EnableTLS(s *Server) {
	s.TLS = true
}
func CustomIdentity(nick, user, real string) ServerOption {
	return func(s *Server) {
		s.Identity = Identity{
			nick, user, real,
		}
	}
}
func DefaultIdentity(s *Server) {
	u := os.Getenv("USER")
	s.Identity = Identity{u, u, u}
}
func Password(pass string) ServerOption {
	return func(s *Server) {
		s.Password = pass
	}
}
func EnableSASL(t SASLType, m SASLMethod) ServerOption {
	return func(s *Server) {
		s.DoSASL = t
		s.AuthMethod = m
	}
}
